//
//  ViewController.swift
//  ThemesTestApp
//
//  Created by Anatoly Nevmerzhitsky on 31.03.2023.
//

import UIKit

class ViewController: UIViewController {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "System text for example"
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textColor = .systemAppRed
        return label
    }()
    private lazy var themeLabel: UILabel = {
        let label = UILabel()
        label.text = "set dark style"
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textColor = .systemPurple
        return label
    }()
    private lazy var segmentedControl: UISegmentedControl = {
        let segmentedControl = UISegmentedControl(items: ["System", "Light", "Dark"])
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(changeThemeStyle), for: .valueChanged)
        return segmentedControl
    }()
    private lazy var themeSwitch: UISwitch = {
       let uiswitch = UISwitch()
        uiswitch.isOn = false
        uiswitch.addTarget(self, action: #selector(changeThemeStyle), for: .valueChanged)
        return uiswitch
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    @objc private func changeThemeStyle(sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        guard let theme = Theme(rawValue: sender.selectedSegmentIndex) else { return }
        theme.setActive()
    }


    private func setupUI(){
        segmentedControl.selectedSegmentIndex = Theme.current.rawValue
        
        view.backgroundColor = .systemBackground
        view.addSubview(titleLabel, anchors: [.centerY(0), .centerX(0)])
        
        view.addSubview(segmentedControl, anchors: [.centerX(0)])
        
//        view.addSubview(themeLabel, anchors: [.leading(16)])
//        view.addSubview(themeSwitch, anchors: [.trailing(-16)])
//
        segmentedControl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50).isActive = true
//        themeSwitch.centerYAnchor.constraint(equalTo: themeLabel.centerYAnchor).isActive = true
    }
}

