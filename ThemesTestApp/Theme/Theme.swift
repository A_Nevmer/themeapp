//
//  Theme.swift
//  ThemesTestApp
//
//  Created by Anatoly Nevmerzhitsky on 31.03.2023.
//

import UIKit

enum Theme: Int, CaseIterable {
    case system = 0
    case light
    case dark
}

extension Theme {
    
    @Persist(key: "app_theme", defaultValue: Theme.system.rawValue)
    private static var appTheme: Int
    
    func save() {
        Theme.appTheme = self.rawValue
    }
    
    static var current: Theme {
        Theme(rawValue: appTheme) ?? .system
    }
}

extension Theme {
    
    @available(iOS 13.0, *)
    
    var userInterfaceStyle: UIUserInterfaceStyle {
        switch self {
        case .system: return themeWindow?.traitCollection.userInterfaceStyle ?? .light
        case .light: return .light
        case .dark: return .dark
        }
    }
    
    func setActive() {
        save()
        
        guard #available(iOS 13.0, *) else { return }

        print("userInterfaceStyle", userInterfaceStyle.rawValue)
        let windows = UIApplication.shared.windows
        print("windows count:", windows.count, " filtered:", windows.filter{ $0 != themeWindow }.count)
        windows.filter{ $0 != themeWindow }.forEach { $0.overrideUserInterfaceStyle = userInterfaceStyle }
//        let allScenes = UIApplication.shared.connectedScenes
//        allScenes.
//        let scene = allScenes.first { $0.activationState == .foregroundActive }
//        if let windowScene = scene as? UIWindowScene {
//            windowScene.keyWindow?.rootViewController?.present(SFSafariViewController(url: url, configuration: conf), animated: isAnimated, completion: nil)
            
//        }
    }
}

extension UIWindow {
    func initTheme() {
        guard #available(iOS 13.0, *) else { return }
        overrideUserInterfaceStyle = Theme.current.userInterfaceStyle
    }
}

@propertyWrapper
struct Persist<T> {
    let key: String
    let defaultValue: T
    
    var wrappedValue: T {
        get { UserDefaults.standard.object(forKey: key) as? T ?? defaultValue }
        set { UserDefaults.standard.set(newValue, forKey: key) }
    }
    
    init(key: String, defaultValue: T) {
        self.key = key
        self.defaultValue = defaultValue
    }
}

//final class ThemeWindow: UIWindow {
//    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
//        if Theme.current == .system {
//            DispatchQueue.main.async {
//                Theme.system.setActive()
//            }
//        }
//    }
//}

final class ThemeWindow: UIWindow {
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if Theme.current == .system {
            Theme.system.setActive()
        }
    }
}
