//
//  UIColor+addition.swift
//  ThemesTestApp
//
//  Created by Anatoly Nevmerzhitsky on 03.04.2023.
//

import UIKit

extension UIColor {
    
    static let lightRed = UIColor(red: 230.0 / 255.0, green: 70.0 / 255.0, blue: 70.0 / 255.0, alpha: 1.0)
    static let darkRed = UIColor(red: 160.0 / 255.0, green: 20.0 / 255.0, blue: 20.0 / 255.0, alpha: 1.0)
    static let systemAppRed = UIColor.color(light: darkRed, dark: lightRed)
    
    static func color(light: UIColor, dark: UIColor) -> UIColor {
        if #available(iOS 13, *) {
            return UIColor.init { traitCollection in
                return traitCollection.userInterfaceStyle == .dark ? dark : light
            }
        } else {
            return light
        }
    }
}
